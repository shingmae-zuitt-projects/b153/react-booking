import {Row, Col, Card} from 'react-bootstrap';
export default function Highlights(){
    return(
        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Learn From Home</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, quos dolore tempora blanditiis eius quia qui nisi error necessitatibus unde commodi ratione nemo quaerat, aperiam corrupti dicta similique illo, alias possimus magni? Ea placeat quia ad beatae delectus tempora adipisci, ipsam laudantium voluptate impedit expedita inventore doloribus id quibusdam error!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Study Now, Pay Later</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, quos dolore tempora blanditiis eius quia qui nisi error necessitatibus unde commodi ratione nemo quaerat, aperiam corrupti dicta similique illo, alias possimus magni? Ea placeat quia ad beatae delectus tempora adipisci, ipsam laudantium voluptate impedit expedita inventore doloribus id quibusdam error!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Be Part of our Community</h2>
                        </Card.Title>
                        <Card.Text>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, quos dolore tempora blanditiis eius quia qui nisi error necessitatibus unde commodi ratione nemo quaerat, aperiam corrupti dicta similique illo, alias possimus magni? Ea placeat quia ad beatae delectus tempora adipisci, ipsam laudantium voluptate impedit expedita inventore doloribus id quibusdam error!
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}