import {Row, Col, Card, Button} from 'react-bootstrap';
export default function CourseCard(){
    return (
        <Row className="mt-3 mb-3">
        <Col lg={12}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h4>Sample Course</h4>
                    </Card.Title>
                    <Card.Text>
                    <p>
                        <b>Description:</b>
                    </p>
                    <p>
                        This is a sample course offering.
                    </p>
                    <p>
                        <b>Price:</b>
                    </p>
                    <p>
                        Php 70,000
                    </p>
                    </Card.Text>
                <Button variant="primary">Enroll</Button>
                </Card.Body>
            </Card>
        </Col>
        </Row>
    )
}